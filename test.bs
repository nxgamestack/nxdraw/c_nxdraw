head

c_flags_fast = -Wall -Werror -march=native -g0 -O2
c_flags_debug = -Wall -Werror -g
c_flags = $c_flags_fast
c_compiler = gcc

rule c_so
  command = $c_compiler -x c -shared -fpic $c_flags -o $out $in

rule c_o
  command = $c_compiler -x c $c_flags -o $out -c $in

rule c_exe
  command = $c_compiler $c_flags -o $out $in

leave

tail

#c_compiler = aarch64-linux-gnu-gcc

#build debug_so_lib: phony build/libnxdraw/so/libnxdraw.so
#  c_flags = $c_flags_debug

build release_so_lib: phony build/libnxdraw/so/libnxdraw.so
  c_flags = $c_flags_fast

build release_o_lib: phony build/libnxdraw/o/libnxdraw.o
  c_flags = $c_flags_fast

default release_o_lib

leave

enter libnxdraw

    source src/_nxdraw/
    
    file c_files nxdraw/api/prefix_graphics.h

    enter so
        merge c_files c_so libnxdraw.so
    leave

    enter o
        merge c_files c_o libnxdraw.o
    leave

leave
